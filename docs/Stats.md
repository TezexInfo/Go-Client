# Stats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StatGroup** | **string** |  | [optional] [default to null]
**Stat** | **string** |  | [optional] [default to null]
**Value** | **string** |  | [optional] [default to null]
**Start** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**End** | [**time.Time**](time.Time.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


