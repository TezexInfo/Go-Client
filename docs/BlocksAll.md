# BlocksAll

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MaxBlockLevel** | **int32** |  | [optional] [default to null]
**Blocks** | [**[]Block**](Block.md) |  | [optional] [default to null]
**TotalResults** | **int32** |  | [optional] [default to null]
**CurrentPage** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


