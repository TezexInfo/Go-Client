# TezosScript

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Int** | **string** |  | [optional] [default to null]
**String** | **string** |  | [optional] [default to null]
**Prim** | **string** |  | [optional] [default to null]
**Args** | [**[]TezosScript**](TezosScript.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


