# NetworkInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MaxLevel** | **int32** |  | [optional] [default to null]
**Blocktime** | **string** |  | [optional] [default to null]
**Transactions24h** | **int32** |  | [optional] [default to null]
**Oeprations24h** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


