# TransactionOperation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Kind** | **string** |  | [optional] [default to null]
**Amount** | **int32** |  | [optional] [default to null]
**Destination** | **string** |  | [optional] [default to null]
**Parameters** | [**TezosScript**](TezosScript.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


