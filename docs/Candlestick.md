# Candlestick

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**O** | **string** |  | [optional] [default to null]
**H** | **string** |  | [optional] [default to null]
**L** | **string** |  | [optional] [default to null]
**C** | **string** |  | [optional] [default to null]
**Vol** | **string** |  | [optional] [default to null]
**Time** | [**time.Time**](time.Time.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


