# Operation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Transaction** | [**Transaction**](Transaction.md) |  | [optional] [default to null]
**Origination** | [**Origination**](Origination.md) |  | [optional] [default to null]
**Delegation** | [**Delegation**](Delegation.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


