# Account

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Address** | **string** |  | [optional] [default to null]
**OperationCount** | **int32** |  | [optional] [default to null]
**SentTransactionCount** | **int32** |  | [optional] [default to null]
**RecvTransactionCount** | **int32** |  | [optional] [default to null]
**OriginationCount** | **int32** |  | [optional] [default to null]
**DelegationCount** | **int32** |  | [optional] [default to null]
**DelegatedCount** | **int32** |  | [optional] [default to null]
**EndorsementCount** | **int32** |  | [optional] [default to null]
**FirstSeen** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**LastSeen** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Balance** | **string** |  | [optional] [default to null]
**TotalSent** | **string** |  | [optional] [default to null]
**TotalReceived** | **string** |  | [optional] [default to null]
**BakedBlocks** | **int32** |  | [optional] [default to null]
**ImageUrl** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


