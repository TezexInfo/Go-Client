# \DelegationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetDelegation**](DelegationApi.md#GetDelegation) | **Get** /delegation/{delegation_hash} | Get Delegation


# **GetDelegation**
> Delegation GetDelegation($delegationHash)

Get Delegation

Get a specific Delegation


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegationHash** | **string**| The hash of the Origination to retrieve | 

### Return type

[**Delegation**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

