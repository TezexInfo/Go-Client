# \OperationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetOperation**](OperationApi.md#GetOperation) | **Get** /operation/{operation_hash} | Get Operation


# **GetOperation**
> Operation GetOperation($operationHash)

Get Operation

Get a specific Operation


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **operationHash** | **string**| The hash of the Operation to retrieve | 

### Return type

[**Operation**](Operation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

