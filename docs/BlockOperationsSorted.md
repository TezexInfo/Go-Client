# BlockOperationsSorted

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Transactions** | [**[]Transaction**](Transaction.md) |  | [optional] [default to null]
**Originations** | [**[]Origination**](Origination.md) |  | [optional] [default to null]
**Delegations** | [**[]Delegation**](Delegation.md) |  | [optional] [default to null]
**Endorsements** | [**[]Endorsement**](Endorsement.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


