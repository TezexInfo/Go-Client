# Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hash** | **string** |  | [optional] [default to null]
**Branch** | **string** |  | [optional] [default to null]
**Source** | **string** |  | [optional] [default to null]
**PublicKey** | **string** |  | [optional] [default to null]
**Level** | **int32** |  | [optional] [default to null]
**BlockHash** | **string** |  | [optional] [default to null]
**Counter** | **int32** |  | [optional] [default to null]
**Time** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Operations** | [**[]TransactionOperation**](TransactionOperation.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


