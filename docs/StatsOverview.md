# StatsOverview

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PriceUsd** | **string** |  | [optional] [default to null]
**PriceBtc** | **string** |  | [optional] [default to null]
**BlockTime** | **int32** | Blocktime in seconds | [optional] [default to null]
**Priority** | **float32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


