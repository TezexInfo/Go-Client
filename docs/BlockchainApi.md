# \BlockchainApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Blockheight**](BlockchainApi.md#Blockheight) | **Get** /maxLevel | Get Max Blockheight


# **Blockheight**
> Level Blockheight()

Get Max Blockheight

Get the maximum Level we have seen


### Parameters
This endpoint does not need any parameter.

### Return type

[**Level**](Level.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

