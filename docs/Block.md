# Block

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hash** | **string** |  | [optional] [default to null]
**NetId** | **string** |  | [optional] [default to null]
**Protocol** | **string** |  | [optional] [default to null]
**Level** | **int32** |  | [optional] [default to null]
**Proto** | **string** |  | [optional] [default to null]
**Successors** | [**[]ChainStatus**](ChainStatus.md) |  | [optional] [default to null]
**Predecessor** | **string** |  | [optional] [default to null]
**Time** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**ValidationPass** | **string** |  | [optional] [default to null]
**Data** | **string** |  | [optional] [default to null]
**ChainStatus** | **string** |  | [optional] [default to null]
**OperationsCount** | **int32** |  | [optional] [default to null]
**OperationsHash** | **string** |  | [optional] [default to null]
**Baker** | **string** |  | [optional] [default to null]
**SeedNonceHash** | **string** |  | [optional] [default to null]
**ProofOfWorkNonce** | **string** |  | [optional] [default to null]
**Signature** | **string** |  | [optional] [default to null]
**Priority** | **int32** |  | [optional] [default to null]
**OperationCount** | **int32** |  | [optional] [default to null]
**TotalFee** | **string** |  | [optional] [default to null]
**Operations** | [**BlockOperationsSorted**](BlockOperationsSorted.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


