# \OriginationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetOrigination**](OriginationApi.md#GetOrigination) | **Get** /origination/{origination_hash} | Get Origination


# **GetOrigination**
> Origination GetOrigination($originationHash)

Get Origination

Get a specific Origination


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **originationHash** | **string**| The hash of the Origination to retrieve | 

### Return type

[**Origination**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

