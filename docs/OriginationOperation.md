# OriginationOperation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Kind** | **string** |  | [optional] [default to null]
**ManagerPubkey** | **string** |  | [optional] [default to null]
**Balance** | **int32** |  | [optional] [default to null]
**Spendable** | **bool** |  | [optional] [default to null]
**Delegateable** | **bool** |  | [optional] [default to null]
**Delegate** | **string** |  | [optional] [default to null]
**Script** | [**TezosScript**](TezosScript.md) |  | [optional] [default to null]
**Storage** | [**TezosScript**](TezosScript.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


