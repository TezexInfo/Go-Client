# Ticker

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BTC** | **string** |  | [optional] [default to null]
**CNY** | **string** |  | [optional] [default to null]
**USD** | **string** |  | [optional] [default to null]
**EUR** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


